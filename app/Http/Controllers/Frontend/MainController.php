<?php

namespace App\Http\Controllers\Frontend;

use App\Model\Category;
use App\Model\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    public function index() {
        $posts = Post::with('user', 'category', 'photo')
            ->where('status', 1)
            ->orderBy('created_at', 'desc')
            ->paginate(2);
        $categories = Category::all();

        return view('frontend.main.index', compact('posts', 'categories'));
    }
}
