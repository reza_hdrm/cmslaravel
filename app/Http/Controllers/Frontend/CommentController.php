<?php

namespace App\Http\Controllers\Frontend;

use App\Model\Comment;
use App\Model\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CommentController extends Controller
{
    public function store(Request $request, $postId) {
        $post = Post::findOrFail($postId);
        if ($post) {
            $dataComment = [
                'description' => $request->input('description'),
                'post_id' => $post->id,
                'status' => 0
            ];
            Comment::create($dataComment);
        }
        Session::flash('add_comment', 'نظر شما با موفقیت درج شد و در انتظار تایید مدیران قرار گرفت');
        return back();
    }

    public function reply(Request $request) {
        $dataComment = $request->all();
        $dataComment['status'] = 0;
        $post = Post::findOrFail($request->input('post_id'));
        if ($post instanceof Post)
            Comment::create($dataComment);
        Session::flash('add_comment', 'نظر شما با موفقیت درج شد و در انتظار تایید مدیران قرار گرفت');
        return back();
    }
}
