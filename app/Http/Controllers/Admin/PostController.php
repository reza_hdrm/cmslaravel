<?php

namespace App\Http\Controllers\Admin;

use App\Model\Category;
use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostEditRequest;
use App\Model\Photo;
use App\Model\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $posts = Post::with('photo', 'category', 'user')->paginate(2);
        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $categories = Category::pluck('title', 'id');
        return view('admin.posts.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostCreateRequest $request) {
        $dataPost = $request->all();
        if ($file = $request->file('first_photo')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $dataPhoto = [
                'path' => $name,
                'name' => $file->getClientOriginalName(),
                'user_id' => Auth::id()
            ];
            $photo = Photo::create($dataPhoto);
            $dataPost['photo_id'] = $photo->id;
        }
        if ($request->input('slug')) {
            $dataPost['slug'] = make_slug($request->input('slug'));
        } else {
            $dataPost['slug'] = make_slug($request->input('title'));
        }
        $dataPost['user_id'] = Auth::id();
        $dataPost['category_id'] = $dataPost['category'];
        unset($dataPost['category'], $dataPost['first_photo']);
        $post = Post::create($dataPost);
        Session::flash('add_post', 'مطلب جدید با موفقیت اضافه شد');
        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $post = Post::with('category')->where('id', $id)->first();
        $categories = Category::pluck('title', 'id');
        return view('admin.posts.edit', compact(['post', 'categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostEditRequest $request, $id) {
        $post = Post::findOrFail($id);
        $photo = Photo::find($post->photo_id);
        $dataPost = $request->all();
        if ($file = $request->file('first_photo')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $dataPhoto = [
                'path' => $name,
                'name' => $file->getClientOriginalName(),
                'user_id' => Auth::id()
            ];
            if ($photo instanceof Photo) {
                unlink(public_path() . $post->photo->path);
                $photo->update($dataPhoto);
            } else {
                $photo = Photo::create($dataPhoto);
                $dataPost['photo_id'] = $photo->id;
            }
        }
        if ($request->input('slug')) {
            $dataPost['slug'] = make_slug($request->input('slug'));
        } else {
            $dataPost['slug'] = make_slug($request->input('title'));
        }
        $dataPost['category_id'] = $dataPost['category'];
        unset($dataPost['category'], $dataPost['first_photo']);
        $post->update($dataPost);
        Session::flash('update_post', 'مطلب جدید با موفقیت ویرایش شد');
        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $post = Post::findOrFail($id);
        if (unlink(public_path() . $post->photo->path))
            $post->delete();
        $photo = Photo::destroy($post->photo_id);
        Session::flash('delete_post', 'مطلب با موفقیت حذف شد');
        return redirect()->route('posts.index');
    }
}
