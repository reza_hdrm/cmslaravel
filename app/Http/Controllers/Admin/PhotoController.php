<?php

namespace App\Http\Controllers\Admin;

use App\Model\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $photos = Photo::with(['user'])->paginate(5);
        return view('admin.photos.index', compact('photos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.photos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $file = $request->file('file');
        $name = time() . $file->getClientOriginalName();
        $file->move('images', $name);
        $dataPhoto = [
            'name' => $file->getClientOriginalName(),
            'path' => $name,
            'user_id' => Auth::id()
        ];
        Photo::create($dataPhoto);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $photo = Photo::findOrFail($id);
        if (unlink(public_path() . $photo->path))
            $photo->delete();
        Session::flash('delete_photo', 'فایل با موفقیت حذف شد');
        return redirect()->route('photos.destroy');
    }

    public function deleteAll(Request $request) {
        $photos = Photo::findOrFail($request->checkBoxArray);

        foreach ($photos as $photo) {
            if (unlink(public_path() . $photo->path))
                $photo->delete();
        }
        Session::flash('delete_photos', 'فایل ها با موفقیت حذف شدند');
        return redirect()->back();
    }
}
