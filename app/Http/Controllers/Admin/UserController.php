<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserEditRequest;
use App\Http\Requests\UsersRequest;
use App\Model\Photo;
use App\Model\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $users = User::with('roles')->paginate(3);
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        $roles = Role::pluck('name', 'id');
        return view('admin.users.create', compact(['roles']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersRequest $request) {
        $dataUser = $request->all();
        if ($file = $request->file('avatar')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $dataPhpto = [
                'path' => $name,
                'name' => $file->getClientOriginalName(),
                'user_id' => Auth::id()
            ];
            $photo = Photo::create($dataPhpto);
            $dataUser['photo_id'] = $photo->id;
        }
        $user = User::create($dataUser);
        $user->roles()->attach($request->input('roles'));
        Session::flash('add_user', 'کاربر جدید با موفقیت اضافه شد');
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $user = User::findOrFail($id);
        $roles = Role::pluck('name', 'id');
        return view('admin.users.edit', compact(['user', 'roles']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserEditRequest $request, $id) {
        $user = User::findORFail($id);
        $photo = Photo::find($user->photo_id);
        $dataUser = $request->all();
        if ($file = $request->file('avatar')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $dataPhpto = [
                'path' => $name,
                'name' => $file->getClientOriginalName(),
                'user_id' => Auth::id()
            ];
            if ($photo instanceof Photo) {
                unlink(public_path() . $user->photo->path);
                $photo->update($dataPhpto);
            } else {
                $photo = Photo::create($dataPhpto);
                $dataUser['photo_id'] = $photo->id;
            }
        }
        if (!$request->input('password'))
            $dataUser['password'] = $user->password;
        $user->update($dataUser);
        $user->roles()->sync($request->input('roles'));
        Session::flash('update_user', 'کاربر با موفقیت ویرایش شد');
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $user = User::findOrFail($id);
        if ($user->photo_id) {
            if (Photo::destroy($user->photo_id)) {
                unlink(public_path() . $user->photo->path);
            }
        }
        $user->delete();
        Session::flash('delete_user', 'کاربر با موفقیت حذف شد');
        return redirect()->route('users.index');
    }
}
