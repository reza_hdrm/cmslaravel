<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!`
|
*/


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'namespace' => 'Admin'], function () {
    Route::resource('users', 'UserController');
    Route::resource('posts', 'PostController');
    Route::resource('categories', 'CategoryController');
    Route::resource('photos', 'PhotoController');
    Route::delete('delete/media', 'PhotoController@deleteAll')->name('photo.delete.all');

    Route::get('dashboard', 'DashboardController@index')->name('dashboard.index');

    Route::get('comments', 'CommentController@index')->name('comments.index');
    Route::post('actions/{id}', 'CommentController@actions')->name('comments.actions');
    Route::get('comments/{id}', 'CommentController@edit')->name('comments.edit');
    Route::patch('comments/{id}', 'CommentController@update')->name('comments.update');
    Route::delete('comments/{id}', 'CommentController@destroy')->name('comments.destroy');
});

Route::group(['name' => 'frontend.', 'namespace' => 'Frontend'], function () {
    Route::get('/', 'MainController@index');
    Route::get('post/{slug}', 'PostController@show')->name('frontend.posts.show');
    Route::get('search', 'PostController@searchTitle')->name('frontend.posts.search');
    Route::post('comments/{postId}', 'CommentController@store')->name('frontend.comments.store');
    Route::post('comments}', 'CommentController@reply')->name('frontend.comments.reply');
});
