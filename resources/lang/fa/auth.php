<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'Login'=>'ورود',
    'Logout'=>'خروج',
    'Register'=>'ثبت‌نام',
    'EmailAddress'=>'آدرس ایمیل',
    'Password'=>'رمز عبور',
    'RememberMe'=>'مرا بخاطر بسپار',
    'ForgetPasswordMe'=>'رمز عبور را فراموش کردم',
    'Name'=>'نام',
    'ConfirmPassword'=>'تایید رمز عبور',
    'Dashboard'=>'داشبورد',
    'You are logged in'=>'شما وارد شده‌اید!',
    'ResetPassword'=>'بازیابی رمزعبور',
    'Send Password Reset Link'=>'ارسال لینک تغییر رمزعبور',

];
